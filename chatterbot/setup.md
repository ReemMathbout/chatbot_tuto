1. First create new conda envirnment to be used
  ```shell script
conda create -n chatbot_env python=3.7
  ```
2. Activate the conda environment
    ```shell script
   conda activate chatbot_env
    ```
3. install the library chatterbot
 ```shell script
pip install chatterbot
```
4. install other dependencies
 ```shell script
pip install -r requirements.txt

pip install chatterbot_corpus
```
5. Install the model of language you to use, here we download the english model of Sapcy
python -m spacy download en